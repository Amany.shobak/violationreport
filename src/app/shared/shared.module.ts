import { CdkTableModule } from '@angular/cdk/table';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatMenuModule } from '@angular/material/menu';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';




import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [ LoadingSpinnerComponent,
     ],
  imports: [
    CommonModule,
    MatTableModule,
    CdkTableModule,
    MatMenuModule,
    InfiniteScrollModule
  ],
  exports: [
    MatTableModule,
    CdkTableModule,
    MatMenuModule,
    LoadingSpinnerComponent , MatDialogModule,InfiniteScrollModule
  ]
})
export class SharedModule { }
