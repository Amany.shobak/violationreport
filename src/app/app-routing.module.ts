import { ReportInfoComponent } from './sandbox/report-info/report-info.component';
import { SpeakUpRequestsComponent } from './sandbox/speak-up-requests/speak-up-requests.component';
import { SpeakUpReplyComponent } from './sandbox/speak-up-reply/speak-up-reply.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddReportComponent } from './sandbox/add-report/add-report.component';

const routes: Routes = [
  {path: 'sandbox', loadChildren: () => import('./sandbox/sandbox-routing.module').then(m => m.SandboxRoutingModule)},
  {
    path: '',
    redirectTo: 'violation-report',
    pathMatch: 'full'
  },
  {path:'add-report', component:AddReportComponent,},
  {path:'speak-up-reply', component:SpeakUpReplyComponent,},
  {path:'speak-up-requests', component:SpeakUpRequestsComponent,},
  {path:'report-info', component:ReportInfoComponent,}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
