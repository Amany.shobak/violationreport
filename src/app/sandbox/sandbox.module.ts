import { SharedModule } from './../shared/shared.module';
import { MatIconModule } from '@angular/material/icon';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SandboxRoutingModule } from './sandbox-routing.module';
import { MaterialModule } from '../material.module';
import { HighchartsChartModule } from 'highcharts-angular';
import { SatPopoverModule } from '@ncstate/sat-popover';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ViolationReportComponent } from './violation-report/violation-report.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AddReportComponent } from './add-report/add-report.component';
import { ViolationComponent } from './violation/violation.component';
import { PopupSuccessComponent } from './popup-success/popup-success.component';
import { DocsUploaderComponent } from './docs-uploader/docs-uploader.component';
import { SpeakUpReplyComponent } from './speak-up-reply/speak-up-reply.component';
import { SpeakUpRequestsComponent } from './speak-up-requests/speak-up-requests.component';

import { DocsTableComponent } from './speak-up-requests/docs-table/docs-table.component';
import { ReportInfoComponent } from './report-info/report-info.component';



@NgModule({
  declarations: [ViolationReportComponent, AddReportComponent, ViolationComponent, 
    PopupSuccessComponent,DocsUploaderComponent, SpeakUpReplyComponent, SpeakUpRequestsComponent, 
     DocsTableComponent, ReportInfoComponent,],
  imports: [
    CommonModule,
    SandboxRoutingModule,
    MaterialModule,
    HighchartsChartModule,
    BrowserAnimationsModule,
    SatPopoverModule,
    SharedModule,
    FontAwesomeModule
  ]
})
export class SandboxModule { }
