
import { Component, OnInit } from '@angular/core';
import { faLongArrowAltRight  } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-violation-report',
  templateUrl: './violation-report.component.html',
  styleUrls: ['./violation-report.component.scss']
})
export class ViolationReportComponent implements OnInit {
  faLongArrowAltRight = faLongArrowAltRight;
  constructor() { }

  ngOnInit(): void {
  }   

}
