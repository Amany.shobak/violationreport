import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeakUpRequestsComponent } from './speak-up-requests.component';

describe('SpeakUpRequestsComponent', () => {
  let component: SpeakUpRequestsComponent;
  let fixture: ComponentFixture<SpeakUpRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpeakUpRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeakUpRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
