import { AfterViewInit, Component, Input, Output, EventEmitter, ViewChild, OnChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';
import { MatSort, Sort } from '@angular/material/sort';

export interface Filters {
  violationTypes: string,
  Owner: string,
  Status: string,

}export interface Owner {
  id: number,
  name: string,
}
export interface violationTypes {
  id: number,
  name: string,
}

export interface Status {
  id: number,
  name: string,
}

export interface PeriodicElement {
  SpeckUpName: string;
  SpeckUp: number;
  Status: string;
  ViolationTypes: string;
  actions:any;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { SpeckUpName: 'Speck up no.1 name', SpeckUp: 76432, Status: 'New', ViolationTypes: 'Criminal',actions:'' },
  { SpeckUpName: 'Speck up no.2 name', SpeckUp: 76432, Status: 'New', ViolationTypes: 'financial' ,actions:'' },
  { SpeckUpName: 'Speck up no.3 name', SpeckUp: 76432, Status: 'Rejected', ViolationTypes: 'managerial',actions:''  },
  { SpeckUpName: 'Speck up no.4 name', SpeckUp: 76432, Status: 'Approved', ViolationTypes: 'internal' ,actions:'' },
  { SpeckUpName: 'Speck up no.1 name', SpeckUp: 76432, Status: 'New', ViolationTypes: 'Criminal',actions:'' },
  { SpeckUpName: 'Speck up no.2 name', SpeckUp: 76432, Status: 'New', ViolationTypes: 'financial' ,actions:'' },
  { SpeckUpName: 'Speck up no.3 name', SpeckUp: 76432, Status: 'Rejected', ViolationTypes: 'managerial',actions:''  },
  { SpeckUpName: 'Speck up no.4 name', SpeckUp: 76432, Status: 'Approved', ViolationTypes: 'internal' ,actions:'' },
];

@Component({
  selector: 'app-docs-table',
  templateUrl: './docs-table.component.html',
  styleUrls: ['./docs-table.component.scss', '../speak-up-requests.component.scss']
})
export class DocsTableComponent implements AfterViewInit {

  displayedColumns: string[] = ['SpeckUpName', 'SpeckUp', 'Status', 'ViolationTypes','actions'];
  dataSource: MatTableDataSource<PeriodicElement>;
  @ViewChild(MatSort) sort: MatSort;
  @Output() sortChangeEmitter = new EventEmitter<Sort>();
  @Output() editEmitter = new EventEmitter();
  @Input() selectedFilters: Filters;
  filterValues = {};

  ViolationTypes: violationTypes[] = [
    { id: 1, name: 'Criminal', },
    { id: 2, name: 'financial', },
    { id: 3, name: 'managerial', },
    { id: 4, name: 'internal', },
  ];
  Owner: Owner[] = [
    { id: 1, name: 'Owner 1', },
    { id: 2, name: 'Owner 2', },
  ];
  Status: Status[] = [
    { id: 1, name: 'Status 1', },
    { id: 2, name: 'Status 2', },
  ];


  faSearch = faSearch;
  faTimes = faTimes;
  @Output() searchQueryEmitter = new EventEmitter();
  @Output() filtersEmitter = new EventEmitter();

  searchQuery: string;




  constructor() {
    this.dataSource = new MatTableDataSource(ELEMENT_DATA);
  }



  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  updateSelectedFilters(filterName: string, filterValues: []) {
    this.selectedFilters[filterName] = filterValues;
    this.filtersEmitter.emit();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  RestFilters() {

    this.dataSource = new MatTableDataSource(ELEMENT_DATA);
  }
}
