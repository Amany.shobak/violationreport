
import { Component, Input, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { fal } from '@fortawesome/fontawesome-free';


@Component({
  selector: 'app-docs-uploader',
  templateUrl: './docs-uploader.component.html',
  styleUrls: ['./docs-uploader.component.scss']
})
export class DocsUploaderComponent implements OnInit, OnChanges {

  faTrashAlt = faTrashAlt;

  @Output() file = new EventEmitter();
  @Input() editMode = false;
  @Input() fileBase64: string | ArrayBuffer;
  @Input() type: string;

  fileBinary: any = { content: null, extension: null, size: null };

  constructor(public dialog: MatDialog,) { }

  ngOnInit() { }

  ngOnChanges() {
    if (this.fileBase64 && this.editMode) {
      const FILE = new File([this.fileBase64], 'download.jpeg', {
        type: 'image/jpeg',
      });
      this.fileBinary.content = FILE;
      this.fileBinary.extension = FILE.type.split('/').pop();
      this.fileBinary.size = (4 * Math.ceil(((this.fileBase64 as string)?.length / 3)) * 0.5624896334383812);
    }
  }

  onHandleFileInput(files: File[]) {
    if (files && files.length > 0) {
      
      this.editMode = false;
      this.fileBinary.content = files[0];
      this.fileBinary.extension = files[0].type.split('/').pop();
      this.fileBinary.size = files[0].size;
      if(this.type === 'img'){
        const fileTypesAllowed = ['jpeg','jpg', 'png','gif'];
        if (fileTypesAllowed.indexOf(this.fileBinary.extension) === -1) {
          /*  this.toast.error('The Extension is not valid','Error');*/
            this.removeFromAttached();
            return false;
          }
      }
      const FILE_READER = new FileReader();
      FILE_READER.readAsDataURL(files[0]);
      FILE_READER.onload = (e: Event) => {
        this.fileBase64 = FILE_READER.result;
      };
      this.file.emit(this.fileBinary);
    }
  }


  removeFromAttached() {
    this.fileBase64 = null;
    this.fileBinary = {
      content: null,
      extension: null,
      size: null
    };
    this.editMode = false;
    this.file.emit(null);
  }
}
