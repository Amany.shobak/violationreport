import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-violation',
  templateUrl: './violation.component.html',
  styleUrls: ['./violation.component.scss']
})
export class ViolationComponent implements OnInit {
  LastFormGroup: FormGroup;
  Cities: any[] = [
    { Name: 'City 1', id: '1' },
    { Name: 'City 2', id: '2' },
  ];
  FullName: string='Mohamed Ahmed Salman';
  FullNames: string[] = [];
  faTimes = faTimes;
  readioSelected:string = 'Yes';
  Choices: string[] = ['Yes', 'No','Don’t know / don’t wish to disclose'];
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.LastFormGroup = this._formBuilder.group({
      FullName: ['',],
      selectionValue: ['', ],
    });

  }
  Add(name: string) {
    if(this.FullName != ''){
      this.FullNames.push(name);
      this.FullName = '';
    }
  };

  removeItem(i: number): void {
    this.FullNames.splice(i, 1);
  }
}
