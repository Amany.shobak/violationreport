import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { faLongArrowAltLeft } from "@fortawesome/free-solid-svg-icons";
import { MatDialog } from '@angular/material/dialog';
import { PopupSuccessComponent } from 'src/app/sandbox/popup-success/popup-success.component';


@Component({
  selector: 'app-add-report',
  templateUrl: './add-report.component.html',
  styleUrls: ['./add-report.component.scss']
})
export class AddReportComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isEditable = true;
  loadingBtn = false;
  readioSelected: string = 'No';
  Choices: string[] = ['Yes', 'No'];
  faLongArrowAltLeft = faLongArrowAltLeft;
  constructor(private _formBuilder: FormBuilder,public dialog: MatDialog,) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      selectionValue: ['',],
    });
    this.secondFormGroup = this._formBuilder.group({

    });
  }

  showComment(){
    const dialogRef = this.dialog.open(PopupSuccessComponent, {
      width: '750px',
      panelClass: 'add-case-mobile-popup',
      data: {title: 'Your Violation Request Summited Successfully ', subtitle: ' We will look into your complaint and we will reply to you soon '}
    });
  }
}
