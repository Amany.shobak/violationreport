import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopupSuccessComponent } from 'src/app/sandbox/popup-success/popup-success.component';
@Component({
  selector: 'app-speak-up-reply',
  templateUrl: './speak-up-reply.component.html',
  styleUrls: ['./speak-up-reply.component.scss']
})
export class SpeakUpReplyComponent implements OnInit {
  loadingBtn = false;
  constructor(public dialog: MatDialog,) { }

  ngOnInit(): void {
  }

  showComment() {
    const dialogRef = this.dialog.open(PopupSuccessComponent, {
      width: '750px',
      panelClass: 'add-case-mobile-popup',
      data: {title: 'Your Violation Reply Sent Successfully ', subtitle: ''}
    });
  }
}
