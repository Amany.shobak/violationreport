import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeakUpReplyComponent } from './speak-up-reply.component';

describe('SpeakUpReplyComponent', () => {
  let component: SpeakUpReplyComponent;
  let fixture: ComponentFixture<SpeakUpReplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpeakUpReplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeakUpReplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
