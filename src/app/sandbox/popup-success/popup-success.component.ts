import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {faTimes} from '@fortawesome/free-solid-svg-icons';


export interface DialogData {
  title: string;
  subtitle: string;
}


@Component({
  selector: 'app-popup-success',
  templateUrl: './popup-success.component.html',
  styleUrls: ['./popup-success.component.scss']
})
export class PopupSuccessComponent implements OnInit {
  faTimes = faTimes;
  constructor(public dialogRef: MatDialogRef<PopupSuccessComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit(): void {
     // console.log(this.data)
  }

  close(){
    this.dialogRef.close({})
  }

}
