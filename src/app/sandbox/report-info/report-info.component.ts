import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-report-info',
  templateUrl: './report-info.component.html',
  styleUrls: ['./report-info.component.scss']
})
export class ReportInfoComponent implements OnInit {
  info: any[] = [
    { name: 'Name:', value: 'Osama Alzahrani' },
    { name: 'Position:', value: 'Project manager' },
    { name: 'Email:', value: 'osama@stc.com.sa' },
    { name: 'Communication time:', value: '14-2-2020  |   04:00pm' },
    { name: 'Communication note:', value: 'I don’t have any comment' },
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
